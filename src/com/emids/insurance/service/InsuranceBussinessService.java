package com.emids.insurance.service;

import com.emids.insurance.model.Person;

public interface InsuranceBussinessService {
	
	Double genericPersonPercentage(Double basePremium,Double Number);
	Double genericPersonAgePercentage(Person p,Double Number);
	Double genericPersonHabitPercentage(Person p,Double Number);
	Double genericPersonHealthPercentage(Person p,Double Number);
	Double genericPersonGenderPercentage(Person p,Double Number);
	

}
