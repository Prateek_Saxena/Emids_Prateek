package com.emids.insurance.model;

public class CurrentHealth {
	
	public boolean ishypertension;
	public boolean isBloodPressure;
	public boolean isBloodSugar;
	public boolean isoverWeight;
	public boolean isIshypertension() {
		return ishypertension;
	}
	public void setIshypertension(boolean ishypertension) {
		this.ishypertension = ishypertension;
	}
	public boolean isBloodPressure() {
		return isBloodPressure;
	}
	public void setBloodPressure(boolean isBloodPressure) {
		this.isBloodPressure = isBloodPressure;
	}
	public boolean isBloodSugar() {
		return isBloodSugar;
	}
	public void setBloodSugar(boolean isBloodSugar) {
		this.isBloodSugar = isBloodSugar;
	}
	public boolean isIsoverWeight() {
		return isoverWeight;
	}
	public void setIsoverWeight(boolean isoverWeight) {
		this.isoverWeight = isoverWeight;
	}
	@Override
	public String toString() {
		return "CurrentHealth [ishypertension=" + ishypertension + ", isBloodPressure=" + isBloodPressure
				+ ", isBloodSugar=" + isBloodSugar + ", isoverWeight=" + isoverWeight + "]";
	}
	
	
	
	
	

}
