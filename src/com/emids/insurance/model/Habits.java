package com.emids.insurance.model;

public class Habits {
	
	public boolean isSmoking;
	public boolean isAlcohol;
	public boolean isDailyExercise;
	public boolean isDrugs;
	public boolean isSmoking() {
		return isSmoking;
	}
	public void setSmoking(boolean isSmoking) {
		this.isSmoking = isSmoking;
	}
	public boolean isAlcohol() {
		return isAlcohol;
	}
	public void setAlcohol(boolean isAlcohol) {
		this.isAlcohol = isAlcohol;
	}
	public boolean isDailyExercise() {
		return isDailyExercise;
	}
	public void setDailyExercise(boolean isDailyExercise) {
		this.isDailyExercise = isDailyExercise;
	}
	public boolean isDrugs() {
		return isDrugs;
	}
	public void setDrugs(boolean isDrugs) {
		this.isDrugs = isDrugs;
	}
	@Override
	public String toString() {
		return "Habits [isSmoking=" + isSmoking + ", isAlcohol=" + isAlcohol + ", isDailyExercise=" + isDailyExercise
				+ ", isDrugs=" + isDrugs + "]";
	}
	
	


}
