package com.emids.insurance.model;

public class Person {
	
	private String name;
	private Gender gender;
	private Long age;
	public CurrentHealth curentHealth=new CurrentHealth();
	public Habits habits=new Habits();
	public Double pemiumAmount;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public Long getAge() {
		return age;
	}
	public void setAge(Long age) {
		this.age = age;
	}
	public CurrentHealth getCurentHealth() {
		return curentHealth;
	}
	public void setCurentHealth(CurrentHealth curentHealth) {
		this.curentHealth = curentHealth;
	}
	public Habits getHabits() {
		return habits;
	}
	public void setHabits(Habits habits) {
		this.habits = habits;
	}
	public Double getPemiumAmount() {
		return pemiumAmount;
	}
	public void setPemiumAmount(Double pemiumAmount) {
		this.pemiumAmount = pemiumAmount;
	}
	@Override
	public String toString() {
		return "Person [name=" + name + ", gender=" + gender + ", age=" + age + ", curentHealth=" + curentHealth
				+ ", habits=" + habits + ", pemiumAmount=" + pemiumAmount + "]";
	}
	
	
	
	
}
