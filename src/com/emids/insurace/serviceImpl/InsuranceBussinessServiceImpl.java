package com.emids.insurace.serviceImpl;

import com.emids.insurance.model.Gender;
import com.emids.insurance.model.Person;
import com.emids.insurance.service.InsuranceBussinessService;

public class InsuranceBussinessServiceImpl implements InsuranceBussinessService{

	@Override
	public Double genericPersonPercentage(Double basePremium, Double Number) {
		return basePremium*Number;
	}

	@Override
	public Double genericPersonAgePercentage(Person p, Double basePremium) {
		
		if(p.getAge()<18) {
			p.pemiumAmount=basePremium;
		}else if(p.getAge()>=18 && p.getAge()<=25){
			p.pemiumAmount= genericPersonPercentage(basePremium, 0.1);
		}
		else if(p.getAge()>=25 && p.getAge()<=30){
			p.pemiumAmount=  genericPersonPercentage(basePremium, 0.1);
		}
		else if(p.getAge()>=30 && p.getAge()<=35){
			p.pemiumAmount=  genericPersonPercentage(basePremium, 0.1);
		}
		else if(p.getAge()>=35 && p.getAge()<=40){
			p.pemiumAmount=  genericPersonPercentage(basePremium, 0.1);
		}
		
		else {
			int years=5;
			
			if(years==5) {
				p.pemiumAmount=  genericPersonPercentage(basePremium, 0.2);
			}else {
				p.pemiumAmount=  basePremium;
			}
			
			
		}

		return p.pemiumAmount;
	}

	@Override
	public Double genericPersonHabitPercentage(Person p, Double basePremium) {
		
		if(p.getHabits().isAlcohol()) {
			p.pemiumAmount += genericPersonPercentage(basePremium,0.03);
			
		}
		if(p.getHabits().isDrugs()) {
			p.pemiumAmount += genericPersonPercentage(basePremium,0.03);
			
		}
		if(p.getHabits().isSmoking()) {
			p.pemiumAmount += genericPersonPercentage(basePremium,0.03);
			
		}
		if(p.getHabits().isDailyExercise()) {
			p.pemiumAmount -= genericPersonPercentage(basePremium,0.03);
			
		}
		return p.pemiumAmount;
	}

	@Override
	public Double genericPersonHealthPercentage(Person p, Double basePremium) {
		
		if (p.getCurentHealth().isBloodPressure()) {
			p.pemiumAmount += genericPersonPercentage(basePremium,0.01);
		}
		if (p.getCurentHealth().isBloodSugar()) {
			p.pemiumAmount += genericPersonPercentage(basePremium,0.01);
		}
		if (p.getCurentHealth().isIshypertension()) {
			p.pemiumAmount += genericPersonPercentage(basePremium,0.01);
		}
		if (p.getCurentHealth().isIsoverWeight()) {
			p.pemiumAmount += genericPersonPercentage(basePremium,0.01);
		}
		System.out
				.println("genericPersonHealthPercentage :"+ p.pemiumAmount);
		return p.pemiumAmount;
	}

	@Override
	public Double genericPersonGenderPercentage(Person p, Double basePremium) {
		
		if (p.getGender().equals(Gender.MALE)) {
			p.pemiumAmount+=genericPersonPercentage(basePremium,0.02);
		}
		return p.pemiumAmount;
	}

		
	

}
