package com.emids.Insurance.MainApplication;


import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.junit.Before;
import org.junit.Test;

import com.emids.insurace.serviceImpl.InsuranceBussinessServiceImpl;
import com.emids.insurance.model.Gender;
import com.emids.insurance.model.Person;

public class FinalSettlementInsuranceTest {
	/*
	 * please test FinalSettlementInsuranceTest run as a junit put the value in console then you are code
	 * generating the result.
	
	*/
	public static final Double base_Premium=5000.0;

	@Before
	public void setUp() throws Exception {
		System.out.println("Before SetUp");
	}

	@Test 
    public void testReult(){  
        
		
		Person p=new Person();
		String gender="";

		try {

			BufferedReader reader = 
					new BufferedReader(new InputStreamReader(System.in));

			System.out.println("Please Enter your Name");
			p.setName(reader.readLine());

			System.out.println("Please Enter your gender(MALE, FEMALE, OTHERS)");
			gender=reader.readLine();

			System.out.println("Please enter you age");
			p.setAge(Long.valueOf(reader.readLine()));

			//setting the value current Health
			//CurrentHealth

			System.out.println("----Your Health Description---");

			System.out.println("HyperTension(Yes/No):");
			p.getCurentHealth().setIshypertension("Yes".equalsIgnoreCase(reader.readLine()) ? true
					: false);

			System.out.println("BloodPressure(Yes/No):");
			p.getCurentHealth().setBloodPressure("Yes".equalsIgnoreCase(reader.readLine()) ? true
					: false);


			System.out.println("BloodSugar(Yes/No):");
			p.getCurentHealth().setBloodSugar("Yes".equalsIgnoreCase(reader.readLine()) ? true
					: false);


			System.out.println("overWeight(Yes/No):");
			p.getCurentHealth().setIsoverWeight("Yes".equalsIgnoreCase(reader.readLine()) ? true
					: false);

			System.out.println("----Your Habit---");

			System.out.println("Alcoholic(Yes/No):");
			p.getHabits().setAlcohol("Yes".equalsIgnoreCase(reader.readLine()) ? true
					: false);


			System.out.println("DailyExercise(Yes/No):");
			p.getHabits().setDailyExercise("Yes".equalsIgnoreCase(reader.readLine()) ? true
					: false);


			System.out.println("Drugs(Yes/No):");
			p.getHabits().setDrugs("Yes".equalsIgnoreCase(reader.readLine()) ? true
					: false);


			System.out.println("Smoking(Yes/No):");
			p.getHabits().setSmoking("Yes".equalsIgnoreCase(reader.readLine()) ? true
					: false);



			if (gender.equalsIgnoreCase("Male")) {
				p.setGender(Gender.MALE);

			} else if (gender.equalsIgnoreCase("Female")) {
				p.setGender(Gender.FEMALE);
			} else {
				p.setGender(Gender.OTHERS);
			}


			System.out.println("Health Insurance Premium for Mr."
					+ p.getName() + ": Rs." + calculateInsurance(p));


		}catch(Exception e){
			e.printStackTrace();
		}


	}

	private static Double calculateInsurance(Person p) {

		InsuranceBussinessServiceImpl impl=new InsuranceBussinessServiceImpl();

		return base_Premium
				+impl.genericPersonAgePercentage(p, base_Premium)
				+impl.genericPersonHabitPercentage(p, base_Premium)
				+impl.genericPersonHealthPercentage(p, base_Premium)
		        +impl.genericPersonGenderPercentage(p, base_Premium);
	}

    }


